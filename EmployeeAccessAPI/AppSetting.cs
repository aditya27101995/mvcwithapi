﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace EmployeeAccessAPI
{
    public class AppSetting
    {
        public static string ApiBaseAddress => GetSettingValue<string>("EmployeeApiToken");

        private static T GetSettingValue<T>(string name)
        {
            string value = ConfigurationManager.AppSettings[name];
            if(value == null)
            {
                throw new Exception("Configuration value not found");
            }
            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            
        }
    }
}