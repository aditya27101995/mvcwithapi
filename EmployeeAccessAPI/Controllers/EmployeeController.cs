﻿using EmployeeAccessAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EmployeeAccessAPI.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Employee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmployeeFormCreate(EmployeeModel employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62893/api/EmployeeDetails");

                var PostTask = client.PostAsJsonAsync<EmployeeModel>("employee", employee);
               // PostTask.Wait();

                var result = PostTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("EmployeeFormCreate");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error, Please contact administrator");
            return View(employee);
        }

    }
}