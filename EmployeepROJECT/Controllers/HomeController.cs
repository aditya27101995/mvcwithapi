﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using EmployeepROJECT.Models;

namespace EmployeepROJECT.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Employee()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Employee(EmployeeModel employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(AppSetting.ApiBaseAddress);

                var PostTask = client.PostAsJsonAsync<EmployeeModel>("EmployeeDetails", employee);
                PostTask.Wait();

                var result = PostTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("EmployeeFormCreate");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error, Please contact administrator");
            return View(employee);
        }
    }
}