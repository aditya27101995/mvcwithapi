﻿using StudentProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;

namespace StudentProject.Controllers
{
    public class EmployeeMVCController : Controller
    {
        private  string BaseAddress = ConfigurationManager.AppSettings["EmployebaseAddress"];
        // GET: EmployeeMVC
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EmployeeFormCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmployeeFormCreate(Employee employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);

                var PostTask = client.PostAsJsonAsync<Employee>("EmployeeDetails", employee);
                PostTask.Wait();

                var result = PostTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("EmployeeFormCreate");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error, Please contact administrator");
            return View(employee);
        }
    }
}