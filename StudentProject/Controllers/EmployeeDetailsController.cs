﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StudentProject.Models;
using System.Data.SqlClient;
using System.Configuration;

using System.Data;


namespace StudentProject.Controllers
{
    public class EmployeeDetailsController : ApiController
    {
        SqlConnection con = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        DataService dataService = new DataService();

        public EmployeeDetailsController()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);

        }

        //Post: api/Employee
        [HttpPost]
        public string Post([FromBody]Employee employee)
        {
            string Message = null;
            cmd = new SqlCommand("SP_EmployeeForm", con);
            cmd.Parameters.AddWithValue("@mode", "Insert");
            cmd.Parameters.AddWithValue("@name", employee.Name);
            cmd.Parameters.AddWithValue("@college", employee.College);
            cmd.Parameters.AddWithValue("@branch", employee.Branch);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            int affectedrow = cmd.ExecuteNonQuery();
            con.Close();
            if (affectedrow > 0)
            {
                Message = "Registration Successfuly";
            }
            else
            {
                Message = "Something is wrong, try agin!";
            }

            return Message;
        }

        
        [HttpPost]
        public List<Employee> EmployeePost([FromBody] Employee values)
        {
            Employee emp = new Employee();
            List<Employee> listemployees = new List<Employee>();
            listemployees = dataService.EmployeeList(values);
            return listemployees;
        }
     
     
    }
}
