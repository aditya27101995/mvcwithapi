﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentProject.Models
{
    public class Employee
    {
        public string Name { get; set; }
        public string College { get; set; }
        public string Branch { get; set; }
    }
}